# Server installation 

mvn liberty:create liberty:install-feature

# server.xml
```xml
<!-- MySQL Library Configuration -->
<library id="mysqlJDBCLib">
    <!--    <fileset dir="${shared.resource.dir}" includes="mysql*.jar"/>-->
    <fileset dir="${server.config.dir}/lib" includes="mysql*.jar"/>
</library>

<!-- Datasource Configuration -->
<dataSource id="mysqljpadatasource" jndiName="jdbc/mysqljpadatasource">
<jdbcDriver libraryRef="mysqlJDBCLib"/>
<properties serverName="localhost" portNumber="3306"
            databaseName="johann"
            user="${db.un}"
            password="${db.pw}"
            serverTimezone="CET"/>
</dataSource>
```

# pom.xml

```xml
  <build>
    <finalName>${project.artifactId}</finalName>
      <plugins>
        <plugin>
          <groupId>io.openliberty.tools</groupId>
          <artifactId>liberty-maven-plugin</artifactId>
          <version>${version.liberty-maven-plugin}</version>
          <configuration>
            <serverName>${project.artifactId}</serverName>
            <copyDependencies>
              <dependencyGroup>
                <!-- Relative to server config directory -->
                <location>lib</location>
                <dependency>
                  <groupId>mysql</groupId>
                  <artifactId>mysql-connector-java</artifactId>
                </dependency>
              </dependencyGroup>
            </copyDependencies>
          </configuration>
        </plugin>
      </plugins>
  </build>
```
