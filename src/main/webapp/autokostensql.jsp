<!DOCTYPE html>
<html lang="de">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<jsp:useBean id="auto" class="de.wsc.Auto"/>

<fmt:setLocale value="de_DE" scope="session"/>

<c:set var="kosten" value="${auto.kostenSql}"/>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title>Autokosten</title>
    <link rel="stylesheet" type="text/css" href="${contextPath}/css/autokosten.css">
    <script src="${contextPath}/js/autokosten.js"></script>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th><label>Datum</label></th>
                <th><label>Betrag</label></th>
                <th><label>Tageskilometer</label></th>
                <th><label>Gesamtkilometer</label></th>
                <th><label>Liter</label></th>
                <th>Durchschnitt</th>
                <th><label>Beschreibung</label></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${kosten}" var="kost">
                <fmt:parseDate  value="${kost.datum}"  type="date" pattern="yyyy-MM-dd" var="parsedDate" />
                <fmt:formatDate value="${parsedDate}" type="date" pattern="dd.MM.yyyy" var="stdDatum" />
            <tr data-id="${kost.id}" class="${lastSavedId == kost.id ? 'lastSaved' : ''}">
                <td data-datum>${stdDatum}</td>
                <td data-betrag><fmt:formatNumber value="${kost.betrag}" pattern="#,##0.00"/></td>
                <td data-tageskilometer><fmt:formatNumber value="${kost.tageskilometer}" pattern="#,##0"/></td>
                <td data-gesamtkilometer><fmt:formatNumber value="${kost.gesamtkilometer}" pattern="#,##0"/></td>
                <td data-liter><fmt:formatNumber value="${kost.liter}" pattern="#,##0.00"/></td>
                <td data-durchschnitt><fmt:formatNumber value="${kost.durchschnitt}" pattern="#,##0.00"/></td>
                <td data-beschreibung>${kost.beschreibung}</td>
                <td>
                    <button name="delete" type="submit" value="${kost.id}">delete</button>
                </td>
            </tr>
            </c:forEach>
        </tbody>
    </table>

    <form id="deleteForm" action="${contextPath}/auto/kosten/delete" method="post">
        <input type="hidden" name="id" />
    </form>

</body>

</html>
