document.addEventListener("DOMContentLoaded", function(event){
    function deleteKosten(evnt) {
        let id = evnt.target.value;
        let deleteForm = document.querySelector("#deleteForm");
        let deleteId = deleteForm.querySelector("input");
        deleteId.value = id;
        deleteForm.submit();
    }

    function selectRow(evnt) {
        let id = evnt.currentTarget.dataset['id'];
        let row = document.querySelector("[data-id='" + id + "']");
        document.querySelector("input[name=id]").value = id;
        copyValue("datum", row);
        copyValue("betrag", row);
        copyValue("tageskilometer", row);
        copyValue("gesamtkilometer", row);
        copyValue("liter", row);
        copyValue("beschreibung", row);

        removeClass("selected");
        row.classList.add("selected");
    }

    function removeClass(cls) {
        let elements = document.querySelectorAll("[class=selected]");
        for (let i = 0; i <elements.length; i++) {
            elements[i].classList.remove(cls);
        }
    }

    function copyValue(key, row) {
        document.querySelector("input[name=" + key + "]").value = row.querySelector("[data-" + key + "]").innerHTML;
    }

    function create() {
        let saveForm = document.querySelector("form[name='saveForm']");
        let id = document.querySelector("input[name='id']");
        id.value = "";
        saveForm.submit();
    }

    function adjustNumbers() {
        let element = document.querySelector("input#tageskilometer");
        element.value = element.value.replace(".", "");
        element = document.querySelector("input#gesamtkilometer");
        element.value = element.value.replace(".", "");
    }

    function durchschnitt() {
        let liter = document.getElementById("liter").value;
        let tageskilometer = document.getElementById("tageskilometer").value;
        liter.replaceAll(",", ".");
        if (liter > 0 && tageskilometer > 0) {
            document.getElementById("durchschnitt").innerText = liter * 100 / tageskilometer;
            // https://stackoverflow.com/questions/3084675/how-does-internationalization-work-in-javascript
        }
    }

    function wire() {
        let i;
        let buttons = document.querySelectorAll("button[name=delete]");
        for (i = 0; i <buttons.length; i++) {
            buttons[i].addEventListener("click", deleteKosten)
        }

        document.querySelector("button[name=create]").addEventListener("click", create);

        let elements = document.querySelectorAll("tr[data-id]");
        for (i = 0; i <buttons.length; i++) {
            elements[i].addEventListener("click", selectRow)
        }

        document.querySelector("form[name=saveForm]").addEventListener("submit", adjustNumbers);
        document.getElementById("tageskilometer").addEventListener("focusout", durchschnitt);
        document.getElementById("liter").addEventListener("focusout", durchschnitt);
    }
    wire();
    let datumElement = document.getElementById("datum");
    datumElement.focus();
    datumElement.select();
});
