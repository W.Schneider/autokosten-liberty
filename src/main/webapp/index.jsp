<html>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="hello" class="de.wsc.Hello"/>

<body>
<h2>${hello.message}</h2>
<h2><a href="autokostenjpa.jsp">AutokostenJPA</a></h2>
<h2><a href="autokostensql.jsp">AutokostenSQL</a></h2>
</body>
</html>
