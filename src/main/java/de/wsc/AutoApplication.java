package de.wsc;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("auto")
public class AutoApplication extends Application {
}
