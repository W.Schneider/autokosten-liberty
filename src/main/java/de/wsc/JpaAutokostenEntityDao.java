package de.wsc;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@RequestScoped
public class JpaAutokostenEntityDao implements Dao<AutokostenEntity> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<AutokostenEntity> get(long id) {
        return Optional.ofNullable(entityManager.find(AutokostenEntity.class, id));
    }

    @Override
    public List<AutokostenEntity> getAll() {
        Query query = entityManager.createQuery("SELECT e FROM AutokostenEntity e");
        return query.getResultList();
    }

    @Override
    public void save(AutokostenEntity user) {
        executeInsideTransaction(entityManager -> entityManager.persist(user));
    }

    @Override
    public void update(AutokostenEntity user, String[] params) {
        executeInsideTransaction(entityManager -> entityManager.merge(user));
    }

    @Override
    public void delete(AutokostenEntity user) {
        executeInsideTransaction(entityManager -> entityManager.remove(user));
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = entityManager.getTransaction();
        try {
            tx.begin();
            action.accept(entityManager);
            tx.commit();
        }
        catch (RuntimeException e) {
            tx.rollback();
            throw e;
        }
    }
}