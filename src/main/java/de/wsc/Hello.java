package de.wsc;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Hello {
    @Inject
    private Greeting greeting;

    public String getMessage() {
        return greeting.greeting();
    }
}
