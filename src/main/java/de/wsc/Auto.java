package de.wsc;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class Auto {

    @Inject
    private JpaAutokostenEntityDao dao;

    @Inject
    private SqlAutokosten sqlAutokosten;

    public List<AutokostenEntity> getKostenJpa() {
        return dao.getAll();
    }

    public List<AutokostenEntity> getKostenSql() {
        return sqlAutokosten.getAll();
    }
}
