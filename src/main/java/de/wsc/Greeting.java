package de.wsc;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;

@RequestScoped
public class Greeting {
    @Resource(name = "jdbc/mysqljpadatasource")
    private DataSource dataSource;

    @Resource(name = "jdbc/h2test")
    private DataSource dataSourceH2;

    public String greeting() {
        return "Hello Wolfgang";
    }
}
