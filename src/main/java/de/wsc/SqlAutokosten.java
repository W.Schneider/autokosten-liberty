package de.wsc;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class SqlAutokosten {

    @Resource(name="java:comp/jdbc/mysqljpadatasource")
    private javax.sql.DataSource dataSource;

    public List<AutokostenEntity> getAll() {
        try (Connection connection = dataSource.getConnection()){
            String sql = "select * from autokosten";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.execute();
                try (ResultSet resultSet = preparedStatement.getResultSet()) {
                    return getAutokostenEntities(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException((e));
        }
    }

    private List<AutokostenEntity> getAutokostenEntities(ResultSet resultSet) throws SQLException {
        List<AutokostenEntity> entities = new ArrayList<>();
        while (resultSet.next()) {
            AutokostenEntity entity = new AutokostenEntity();
            entity.setId(resultSet.getLong("id"));
            entity.setBeschreibung(resultSet.getString("beschreibung"));
            entity.setBetrag(resultSet.getBigDecimal("betrag"));
            entity.setDatum(resultSet.getTimestamp("datum"));
            entity.setGesamtkilometer(resultSet.getInt("gesamtkilometer"));
            entity.setLiter(resultSet.getBigDecimal("liter"));
            entity.setTageskilometer(resultSet.getInt("tageskilometer"));
            entity.setTags(resultSet.getString("tags"));
            entities.add(entity);
        }
        return entities;
    }
}
