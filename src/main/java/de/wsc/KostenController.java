package de.wsc;

import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.View;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Controller
@Path("kosten")
public class KostenController {
    @Inject
    Models models;

    @GET
    @View("/autokostensql.jsp")
    @Produces("text/html; charset=UTF-8")
    public void autokosten() {

    }

    @POST
    @View("redirect:/kosten")
    @Path("create")
    public void create() {

    }

    @POST
    @View("redirect:/kosten")
    @Path("delete")
    public void delete() {

    }

}
